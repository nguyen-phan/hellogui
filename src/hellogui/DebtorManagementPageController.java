package hellogui;

import hellogui.DebtorManagementPage.DebtorManagementPageData;

import javax.faces.event.ComponentSystemEvent;

import ch.axonivy.fintech.standard.guiframework.bean.GuiFrameworkManagedBean;
import ch.axonivy.fintech.standard.guiframework.core.BaseGuiFrameworkController;
import ch.axonivy.fintech.standard.guiframework.core.GuiFrameworkControllerConfig;
import ch.axonivy.fintech.standard.guiframework.exception.GuiFrameworkException;
import ch.axonivy.fintech.standard.guiframework.util.GuiFrameworkUtil;

public class DebtorManagementPageController extends BaseGuiFrameworkController<DebtorManagementPageData> {
	
	private DebtorManagementPageController(GuiFrameworkControllerConfig<DebtorManagementPageData> config) {
		super(config);
	}
	
	public static DebtorManagementPageController createInstance(GuiFrameworkControllerConfig<DebtorManagementPageData> config) {
		return new DebtorManagementPageController(config);
	}
	
	@Override
	public void doPreRenderViewEvent(ComponentSystemEvent event, DebtorManagementPageData object) throws GuiFrameworkException {
		//Get GUI-FW managed bean
		GuiFrameworkManagedBean guiBean = GuiFrameworkUtil.getGuiFrameworkManagedBean(); 
		
		//Access additional rule data the we register in the GUI-FW config
		DebtorManagementPageAdditionalData debtorManagementPageAdditionalData = 
				(DebtorManagementPageAdditionalData) guiBean.getPageContext().getExternalRuleAdditionalDatas().get("debtorManagementPageAdditionalData");
		
		//Set to additional rule data with the data we get from page data
		debtorManagementPageAdditionalData.setReadonlyMode(object.getIsReadonly());
		
		super.doPreRenderViewEvent(event, object);
	}
}
