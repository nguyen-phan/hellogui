package hellogui;

public class DebtorManagementPageAdditionalData {
	
	private boolean readonlyMode;

	public boolean isReadonlyMode() {
		return readonlyMode;
	}

	public void setReadonlyMode(boolean readonlyMode) {
		this.readonlyMode = readonlyMode;
	}
}
