package hellogui.listenner;

import hellogui.DebtorManagement;

import java.util.Map;

import javax.faces.event.ValueChangeEvent;

import ch.axonivy.fintech.standard.guiframework.changedobserver.eventlistener.NotifyValueChangeEventListener;

public class IsHavingPartnerDebtorValueListener implements NotifyValueChangeEventListener {
	
	@Override
	public void execute(ValueChangeEvent event, Map<String, Object> params) throws Exception {
		DebtorManagement debtorManagement = (DebtorManagement) params.get(PARAM_DOSSIER_DATA);
		
		debtorManagement.getPartner().setName(null);
		debtorManagement.getPartner().setAge(null);	
	}
}
