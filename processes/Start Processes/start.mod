[Ivy]
169B99FE0A2A3862 3.20 #module
>Proto >Proto Collection #zClass
st0 start Big #zClass
st0 B #cInfo
st0 #process
st0 @TextInP .resExport .resExport #zField
st0 @TextInP .type .type #zField
st0 @TextInP .processKind .processKind #zField
st0 @AnnotationInP-0n ai ai #zField
st0 @MessageFlowInP-0n messageIn messageIn #zField
st0 @MessageFlowOutP-0n messageOut messageOut #zField
st0 @TextInP .xml .xml #zField
st0 @TextInP .responsibility .responsibility #zField
st0 @StartRequest f0 '' #zField
st0 @EndTask f1 '' #zField
st0 @RichDialog f3 '' #zField
st0 @PushWFArc f4 '' #zField
st0 @PushWFArc f2 '' #zField
st0 @StartRequest f5 '' #zField
st0 @EndTask f6 '' #zField
st0 @RichDialog f8 '' #zField
st0 @PushWFArc f7 '' #zField
st0 @RichDialog f10 '' #zField
st0 @PushWFArc f11 '' #zField
st0 @PushWFArc f9 '' #zField
>Proto st0 st0 start #zField
st0 f0 outLink start.ivp #txt
st0 f0 type hellogui.Data #txt
st0 f0 inParamDecl '<> param;' #txt
st0 f0 actionDecl 'hellogui.Data out;
' #txt
st0 f0 guid 169B99FE0A42E0FD #txt
st0 f0 requestEnabled true #txt
st0 f0 triggerEnabled false #txt
st0 f0 callSignature start() #txt
st0 f0 caseData businessCase.attach=true #txt
st0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
st0 f0 @C|.responsibility Everybody #txt
st0 f0 81 49 30 30 -21 17 #rect
st0 f0 @|StartRequestIcon #fIcon
st0 f1 type hellogui.Data #txt
st0 f1 337 49 30 30 0 15 #rect
st0 f1 @|EndIcon #fIcon
st0 f3 targetWindow NEW #txt
st0 f3 targetDisplay TOP #txt
st0 f3 richDialogId hellogui.LeaveRequestPage #txt
st0 f3 startMethod start() #txt
st0 f3 type hellogui.Data #txt
st0 f3 requestActionDecl '<> param;' #txt
st0 f3 responseActionDecl 'hellogui.Data out;
' #txt
st0 f3 responseMappingAction 'out=in;
' #txt
st0 f3 isAsynch false #txt
st0 f3 isInnerRd false #txt
st0 f3 userContext '* ' #txt
st0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>LeaveRequest</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
st0 f3 168 42 112 44 -40 -8 #rect
st0 f3 @|RichDialogIcon #fIcon
st0 f4 expr out #txt
st0 f4 111 64 168 64 #arcP
st0 f2 expr out #txt
st0 f2 280 64 337 64 #arcP
st0 f5 outLink startDebtorManagement.ivp #txt
st0 f5 type hellogui.Data #txt
st0 f5 inParamDecl '<> param;' #txt
st0 f5 actionDecl 'hellogui.Data out;
' #txt
st0 f5 guid 169F1C6F000BC42D #txt
st0 f5 requestEnabled true #txt
st0 f5 triggerEnabled false #txt
st0 f5 callSignature startDebtorManagement() #txt
st0 f5 persist false #txt
st0 f5 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
st0 f5 caseData businessCase.attach=true #txt
st0 f5 showInStartList 1 #txt
st0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startDebtorManagement</name>
        <nameStyle>21,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
st0 f5 @C|.responsibility Everybody #txt
st0 f5 81 145 30 30 -66 17 #rect
st0 f5 @|StartRequestIcon #fIcon
st0 f6 type hellogui.Data #txt
st0 f6 513 145 30 30 0 15 #rect
st0 f6 @|EndIcon #fIcon
st0 f8 targetWindow NEW #txt
st0 f8 targetDisplay TOP #txt
st0 f8 richDialogId hellogui.DebtorManagementPage #txt
st0 f8 startMethod start(Boolean) #txt
st0 f8 type hellogui.Data #txt
st0 f8 requestActionDecl '<Boolean isReadonly> param;' #txt
st0 f8 requestMappingAction 'param.isReadonly=in.isReadonly;
' #txt
st0 f8 responseActionDecl 'hellogui.Data out;
' #txt
st0 f8 responseMappingAction 'out=in;
' #txt
st0 f8 isAsynch false #txt
st0 f8 isInnerRd false #txt
st0 f8 userContext '* ' #txt
st0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>DebtorManagementPage</name>
        <nameStyle>20,7
</nameStyle>
    </language>
</elementInfo>
' #txt
st0 f8 336 136 160 48 -69 -8 #rect
st0 f8 @|RichDialogIcon #fIcon
st0 f7 expr out #txt
st0 f7 496 160 513 160 #arcP
st0 f10 targetWindow NEW #txt
st0 f10 targetDisplay TOP #txt
st0 f10 richDialogId hellogui.ReadOnlyDialog #txt
st0 f10 startMethod start() #txt
st0 f10 type hellogui.Data #txt
st0 f10 requestActionDecl '<> param;' #txt
st0 f10 responseActionDecl 'hellogui.Data out;
' #txt
st0 f10 responseMappingAction 'out=in;
out.isReadonly=result.isReadonly;
' #txt
st0 f10 isAsynch false #txt
st0 f10 isInnerRd false #txt
st0 f10 userContext '* ' #txt
st0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Readonly Dialog</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
st0 f10 168 138 112 44 -45 -8 #rect
st0 f10 @|RichDialogIcon #fIcon
st0 f11 expr out #txt
st0 f11 111 160 168 160 #arcP
st0 f9 expr out #txt
st0 f9 280 160 336 160 #arcP
>Proto st0 .type hellogui.Data #txt
>Proto st0 .processKind NORMAL #txt
>Proto st0 0 0 32 24 18 0 #rect
>Proto st0 @|BIcon #fIcon
st0 f0 mainOut f4 tail #connect
st0 f4 head f3 mainIn #connect
st0 f3 mainOut f2 tail #connect
st0 f2 head f1 mainIn #connect
st0 f8 mainOut f7 tail #connect
st0 f7 head f6 mainIn #connect
st0 f5 mainOut f11 tail #connect
st0 f11 head f10 mainIn #connect
st0 f10 mainOut f9 tail #connect
st0 f9 head f8 mainIn #connect
