[Ivy]
16A484E249072A40 3.20 #module
>Proto >Proto Collection #zClass
Rs0 ReadOnlyDialogProcess Big #zClass
Rs0 RD #cInfo
Rs0 #process
Rs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Rs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Rs0 @TextInP .resExport .resExport #zField
Rs0 @TextInP .type .type #zField
Rs0 @TextInP .processKind .processKind #zField
Rs0 @AnnotationInP-0n ai ai #zField
Rs0 @MessageFlowInP-0n messageIn messageIn #zField
Rs0 @MessageFlowOutP-0n messageOut messageOut #zField
Rs0 @TextInP .xml .xml #zField
Rs0 @TextInP .responsibility .responsibility #zField
Rs0 @RichDialogInitStart f0 '' #zField
Rs0 @RichDialogProcessEnd f1 '' #zField
Rs0 @PushWFArc f2 '' #zField
Rs0 @RichDialogProcessStart f3 '' #zField
Rs0 @RichDialogProcessStart f4 '' #zField
Rs0 @RichDialogEnd f5 '' #zField
Rs0 @RichDialogEnd f6 '' #zField
Rs0 @PushWFArc f7 '' #zField
Rs0 @PushWFArc f8 '' #zField
>Proto Rs0 Rs0 ReadOnlyDialogProcess #zField
Rs0 f0 guid 16A484E24CAB8113 #txt
Rs0 f0 type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
Rs0 f0 method start() #txt
Rs0 f0 disableUIEvents true #txt
Rs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Rs0 f0 outParameterDecl '<java.lang.Boolean isReadonly> result;
' #txt
Rs0 f0 outParameterMapAction 'result.isReadonly=in.isReadonly;
' #txt
Rs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rs0 f0 83 51 26 26 -16 15 #rect
Rs0 f0 @|RichDialogInitStartIcon #fIcon
Rs0 f1 type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
Rs0 f1 211 51 26 26 0 12 #rect
Rs0 f1 @|RichDialogProcessEndIcon #fIcon
Rs0 f2 expr out #txt
Rs0 f2 109 64 211 64 #arcP
Rs0 f3 guid 16A4853B47512D50 #txt
Rs0 f3 type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
Rs0 f3 actionDecl 'hellogui.ReadOnlyDialog.ReadOnlyDialogData out;
' #txt
Rs0 f3 actionTable 'out=in;
' #txt
Rs0 f3 actionCode 'out.isReadonly = true;' #txt
Rs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>readonly</name>
        <nameStyle>8,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rs0 f3 83 147 26 26 -23 15 #rect
Rs0 f3 @|RichDialogProcessStartIcon #fIcon
Rs0 f4 guid 16A4853C116556A0 #txt
Rs0 f4 type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
Rs0 f4 actionDecl 'hellogui.ReadOnlyDialog.ReadOnlyDialogData out;
' #txt
Rs0 f4 actionTable 'out=in;
' #txt
Rs0 f4 actionCode 'out.isReadonly = false;' #txt
Rs0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>edit</name>
        <nameStyle>4,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rs0 f4 83 243 26 26 -10 15 #rect
Rs0 f4 @|RichDialogProcessStartIcon #fIcon
Rs0 f5 type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
Rs0 f5 guid 16A4854254E7BBA4 #txt
Rs0 f5 211 147 26 26 0 12 #rect
Rs0 f5 @|RichDialogEndIcon #fIcon
Rs0 f6 type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
Rs0 f6 guid 16A4854330ED6A1B #txt
Rs0 f6 211 243 26 26 0 12 #rect
Rs0 f6 @|RichDialogEndIcon #fIcon
Rs0 f7 expr out #txt
Rs0 f7 109 160 211 160 #arcP
Rs0 f8 expr out #txt
Rs0 f8 109 256 211 256 #arcP
>Proto Rs0 .type hellogui.ReadOnlyDialog.ReadOnlyDialogData #txt
>Proto Rs0 .processKind HTML_DIALOG #txt
>Proto Rs0 -8 -8 16 16 16 26 #rect
>Proto Rs0 '' #fIcon
Rs0 f0 mainOut f2 tail #connect
Rs0 f2 head f1 mainIn #connect
Rs0 f3 mainOut f7 tail #connect
Rs0 f7 head f5 mainIn #connect
Rs0 f4 mainOut f8 tail #connect
Rs0 f8 head f6 mainIn #connect
